// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package elvenshtein

import (
	"unicode/utf8"

	"gitlab.com/opennota/elvenshtein/util"
)

// Distance returns the Levenshtein distance between the two unicode strings.
func Distance(s, t string) int {
	if s == t {
		return 0
	}
	if s == "" {
		return utf8.RuneCountInString(t)
	}
	if t == "" {
		return utf8.RuneCountInString(s)
	}

	source, target := []rune(s), []rune(t)

	height := len(source)
	width := len(target)

	// Strip common prefix
	i := 0
	m := min(width, height)
	for i < m && target[i] == source[i] {
		i++
	}
	source, target = source[i:], target[i:]
	height -= i
	width -= i

	// Strip common suffix
	for height > 0 && width > 0 && source[height-1] == target[width-1] {
		height--
		width--
	}

	if height == 0 {
		return width
	}

	if width == 0 {
		return height
	}

	row := make([]int, width+1)

	for i := 1; i <= width; i++ {
		row[i] = i
	}
	for i := 1; i <= height; i++ {
		row[0] = i
		lastdiag := i - 1
		for j := 1; j <= width; j++ {
			olddiag := row[j]
			inc := 0
			if source[i-1] != target[j-1] {
				inc++
			}
			row[j] = min(row[j]+1, min(row[j-1]+1, lastdiag+inc))
			lastdiag = olddiag
		}
	}
	return row[width]
}

// DistanceScript returns the Levenshtein distance between the two unicode strings
// and a sequence of edit steps needed to transform the first string into the second.
func DistanceScript(s, t string) (int, EditScript) {
	if s == t {
		return 0, nil
	}

	source, target := []rune(s), []rune(t)

	height := len(source) + 1
	width := len(target) + 1
	distanceMatrix := util.NewMatrix(height, width)
	backtraceMatrix := util.NewMatrix(height, width)

	// Initialize first rows.
	for j := 1; j < width; j++ {
		distanceMatrix[0][j] = j
		backtraceMatrix[0][j] = left
	}
	// Initialize first columns.
	for i := 1; i < height; i++ {
		distanceMatrix[i][0] = i
		backtraceMatrix[i][0] = up
	}

	// Fill in the remaining cells.
	for i := 1; i < height; i++ {
		for j := 1; j < width; j++ {
			del := distanceMatrix[i-1][j] + 1
			ins := distanceMatrix[i][j-1] + 1
			repl := distanceMatrix[i-1][j-1]
			if source[i-1] != target[j-1] {
				repl++
			}

			min := min(del, min(ins, repl))

			distanceMatrix[i][j] = min

			switch min {
			case del:
				backtraceMatrix[i][j] = up
			case ins:
				backtraceMatrix[i][j] = left
			default:
				backtraceMatrix[i][j] = upLeft
			}
		}
	}
	distance := distanceMatrix[height-1][width-1]

	i := height - 1
	j := width - 1
	var script EditScript
	// Backtrack.
	for i >= 0 && j >= 0 && !(i == 0 && j == 0) {
		switch backtraceMatrix[i][j] {
		case up:
			i--
			script = append(script, EditStep{i, Del, source[i], 0})
		case left:
			j--
			script = append(script, EditStep{i, Ins, target[j], 0})
		case upLeft:
			i--
			j--
			if source[i] == target[j] {
				continue
			}
			script = append(script, EditStep{i, Repl, source[i], target[j]})
		default:
			panic("invalid direction")
		}
	}

	// Reverse the steps in place.
	for k, l := 0, len(script)-1; k < len(script)/2; k, l = k+1, l-1 {
		script[k], script[l] = script[l], script[k]
	}

	return distance, script
}
