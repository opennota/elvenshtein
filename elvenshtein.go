// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package elvenshtein provides functions for calculating Levenshtein distances between unicode strings and string slices.
package elvenshtein

// EditOp is an edit operation, one of Ins, Del and Repl.
type EditOp byte

const (
	Ins  EditOp = iota + 1 // Insert operation
	Del                    // Delete operation
	Repl                   // Replace operation

	up = iota + 1
	left
	upLeft
)

// String returns the string representation of an edit operation.
func (op EditOp) String() string {
	switch op {
	case Ins:
		return "ins"
	case Del:
		return "del"
	case Repl:
		return "repl"
	default:
		panic("invalid op")
	}
}

// EditStep is an edit step.
type EditStep struct {
	Index  int    // index at which the edit is to be performed
	Op     EditOp // edit operation (Ins, Del or Repl)
	Char   rune   // rune which is to be inserted, deleted or replaced
	ToChar rune   // replacement rune (for the Repl operation)
}

// EditScript is a sequence of edit steps.
type EditScript []EditStep

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
