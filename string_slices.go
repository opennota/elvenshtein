// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package elvenshtein

func slicesEqual(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, s := range a {
		if s != b[i] {
			return false
		}
	}
	return true
}

// DistanceSlices returns the Levenshtein distance between the two string slices.
func DistanceSlices(s, t []string) int {
	if slicesEqual(s, t) {
		return 0
	}
	if len(s) == 0 {
		return len(t)
	}
	if len(t) == 0 {
		return len(s)
	}

	height := len(s)
	width := len(t)

	// Strip common prefix
	i := 0
	m := min(width, height)
	for i < m && s[i] == t[i] {
		i++
	}
	s, t = s[i:], t[i:]
	height -= i
	width -= i

	// Strip common suffix
	for height > 0 && width > 0 && s[height-1] == t[width-1] {
		height--
		width--
	}

	if height == 0 {
		return width
	}

	if width == 0 {
		return height
	}

	row := make([]int, width+1)

	for i := 1; i <= width; i++ {
		row[i] = i
	}
	for i := 1; i <= height; i++ {
		row[0] = i
		lastdiag := i - 1
		for j := 1; j <= width; j++ {
			olddiag := row[j]
			inc := 0
			if s[i-1] != t[j-1] {
				inc++
			}
			row[j] = min(row[j]+1, min(row[j-1]+1, lastdiag+inc))
			lastdiag = olddiag
		}
	}
	return row[width]
}
