// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package elvenshtein

import (
	"reflect"
	"strings"
	"testing"
)

var testCases = []struct {
	source, target string
	d              int
	script         EditScript
}{
	{"", "", 0, nil},
	{"a", "a", 0, nil},
	{"", "a", 1, EditScript{{0, Ins, 'a', 0}}},
	{"a", "aa", 1, EditScript{{1, Ins, 'a', 0}}},
	{"a", "aab", 2, EditScript{{1, Ins, 'a', 0}, {1, Ins, 'b', 0}}},
	{"a", "b", 1, EditScript{{0, Repl, 'a', 'b'}}},
	{"aaa", "aba", 1, EditScript{{1, Repl, 'a', 'b'}}},
	{"ab", "ab", 0, nil},
	{"a", "", 1, EditScript{{0, Del, 'a', 0}}},
	{"aa", "a", 1, EditScript{{1, Del, 'a', 0}}},
	{"aaa", "a", 2, EditScript{{1, Del, 'a', 0}, {2, Del, 'a', 0}}},
	{"kitten", "sitting", 3, EditScript{{0, Repl, 'k', 's'}, {4, Repl, 'e', 'i'}, {6, Ins, 'g', 0}}},
	{"flaw", "lawn", 2, EditScript{{0, Del, 'f', 0}, {4, Ins, 'n', 0}}},
	{"Saturday", "Sunday", 3, EditScript{{1, Del, 'a', 0}, {2, Del, 't', 0}, {4, Repl, 'r', 'n'}}},
	{"levenshtein", "elvenshtein", 2, EditScript{{0, Ins, 'e', 0}, {1, Del, 'e', 0}}},
}

func TestDistance(t *testing.T) {
	for _, tc := range testCases {
		d := Distance(tc.source, tc.target)
		if d != tc.d {
			t.Errorf("Distance(%q, %q) returned distance %d; want %d", tc.source, tc.target, d, tc.d)
		}
	}
}

func TestDistanceScript(t *testing.T) {
	for _, tc := range testCases {
		d, script := DistanceScript(tc.source, tc.target)
		if d != tc.d {
			t.Errorf("DistanceScript(%q, %q) returned distance %d; want %d", tc.source, tc.target, d, tc.d)
		}
		if !reflect.DeepEqual(script, tc.script) {
			t.Errorf("DistanceScript(%q, %q) returned script %v; want %v", tc.source, tc.target, script, tc.script)
		}
	}
}

func BenchmarkDistance(b *testing.B) {
	for i := 0; i < b.N; i++ {
		tc := testCases[i%len(testCases)]
		Distance(tc.source, tc.target)
	}
}

func TestDistanceSlices(t *testing.T) {
	testCases := []struct {
		a, b []string
		want int
	}{
		{strings.Split("", " "), strings.Split("", " "), 0},
		{strings.Split("a", " "), strings.Split("a", " "), 0},
		{strings.Split("a", " "), strings.Split("a a", " "), 1},
		{strings.Split("a", " "), strings.Split("a a b", " "), 2},
		{strings.Split("a", " "), strings.Split("b", " "), 1},
		{strings.Split("a a a", " "), strings.Split("a b a", " "), 1},
		{strings.Split("a b", " "), strings.Split("a b", " "), 0},
		{strings.Split("a", " "), strings.Split("", " "), 1},
		{strings.Split("a a", " "), strings.Split("a", " "), 1},
		{strings.Split("a a a", " "), strings.Split("a", " "), 2},
		{strings.Split("a a a", " "), strings.Split("a", " "), 2},
		{strings.Split("k i t t e n", " "), strings.Split("s i t t i n g", " "), 3},
		{strings.Split("f l a w", " "), strings.Split("l a w n", " "), 2},
		{strings.Split("S a t u r d a y", " "), strings.Split("S u n d a y", " "), 3},
		{strings.Split("a b", " "), strings.Split("a c d b", " "), 2},
	}

	for _, tc := range testCases {
		d := DistanceSlices(tc.a, tc.b)
		if d != tc.want {
			t.Errorf("DistanceSlices(%v, %v) = %d, want %d", tc.a, tc.b, d, tc.want)
		}
	}
}
