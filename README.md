elvenshtein [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](http://godoc.org/gitlab.com/opennota/elvenshtein?status.svg)](http://godoc.org/gitlab.com/opennota/elvenshtein) [![Pipeline status](https://gitlab.com/opennota/elvenshtein/badges/master/pipeline.svg)](https://gitlab.com/opennota/elvenshtein/commits/master)

Package elvenshtein provides functions for calculating Levenshtein distances between unicode strings and string slices.

## Install

    go get -u gitlab.com/opennota/elvenshtein

## Other implementations

- https://github.com/agext/levenshtein
- https://github.com/agnivade/levenshtein
- https://github.com/arbovm/levenshtein
- https://github.com/dgryski/trifles/leven
- https://github.com/honzab/levenshtein
- https://github.com/jancajthaml-go/levenstein
- https://github.com/kse/levenshtein
- https://github.com/m1ome/leven
- https://github.com/SteveFortune/go-levenshtein
- https://github.com/texttheater/golang-levenshtein
