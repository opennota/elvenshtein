// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package util provides functions for matrix allocation.
package util

// NewMatrix returns a new zero matrix with m rows and n columns.
func NewMatrix(m, n int) [][]int {
	mat := make([][]int, m)
	alloc := make([]int, m*n)
	for i, j := 0, 0; i < m; i, j = i+1, j+n {
		mat[i] = alloc[j : j+n : j+n]
	}
	return mat
}
