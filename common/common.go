// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package common helps to find largest common prefixes, suffixes and substrings of two strings.
package common

import (
	"unicode/utf8"

	"gitlab.com/opennota/elvenshtein/util"
)

// Prefix returns the largest common prefix of the two strings and its length in runes.
func Prefix(a, b string) (string, int) {
	i := 0
	n := 0
	for {
		r, size := utf8.DecodeRuneInString(a[i:])
		if size == 0 {
			break
		}
		r2, size2 := utf8.DecodeRuneInString(b[i:])
		if size2 == 0 {
			break
		}
		if r != r2 {
			break
		}
		if r == utf8.RuneError && r2 == utf8.RuneError {
			break
		}
		n++
		i += size
	}
	return a[:i], n
}

// Suffix returns the largest common suffix of the two strings and its length in runes.
func Suffix(a, b string) (string, int) {
	i := len(a)
	j := len(b)
	n := 0
	for {
		r, size := utf8.DecodeLastRuneInString(a[:i])
		if size == 0 {
			break
		}
		r2, size2 := utf8.DecodeLastRuneInString(b[:j])
		if size2 == 0 {
			break
		}
		if r != r2 {
			break
		}
		if r == utf8.RuneError && r2 == utf8.RuneError {
			break
		}
		n++
		i -= size
		j -= size
	}
	return a[i:], n
}

// Substring returns the largest common substring of the two strings and its length in runes.
func Substring(a, b string) (string, int) {
	if a == "" || b == "" {
		return "", 0
	}

	ra, rb := []rune(a), []rune(b)
	m := util.NewMatrix(len(ra)+1, len(rb)+1)

	maxLength := 0
	maxIndex := 0
	for i := 0; i < len(ra); i++ {
		for j := 0; j < len(rb); j++ {
			if ra[i] != rb[j] {
				continue
			}
			n := m[i][j] + 1
			if n > maxLength {
				maxIndex = i + 1
				maxLength = n
			}
			m[i+1][j+1] = n
		}
	}
	return string(ra[maxIndex-maxLength : maxIndex]), maxLength
}
