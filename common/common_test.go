// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package common

import (
	"testing"
	"unicode/utf8"
)

func TestPrefix(t *testing.T) {
	testcases := []struct {
		a, b string
		want string
	}{
		{"", "", ""},
		{"ф", "", ""},
		{"", "ф", ""},
		{"э", "ф", ""},
		{"эээ", "ф", ""},
		{"ф", "эээ", ""},
		{"эльф", "эй", "э"},
		{"ольга", "оля", "ол"},
		{"астра", "астрал", "астра"},
		{"река", "река", "река"},
	}
	for _, tc := range testcases {
		got, n := Prefix(tc.a, tc.b)
		rcWant := utf8.RuneCountInString(tc.want)
		rcGot := utf8.RuneCountInString(got)
		if n != rcGot || rcGot != rcWant || got != tc.want {
			t.Errorf("Prefix(%q,%q) = %d,%q, want %d,%q", tc.a, tc.b, n, got, rcWant, tc.want)
		}
	}
}

func TestSuffix(t *testing.T) {
	testcases := []struct {
		a, b string
		want string
	}{
		{"", "", ""},
		{"ф", "", ""},
		{"", "ф", ""},
		{"э", "ф", ""},
		{"эээ", "ф", ""},
		{"ф", "эээ", ""},
		{"эльф", "эй", ""},
		{"ольга", "оля", ""},
		{"кадастр", "пиастр", "астр"},
		{"фантаст", "поддаст", "аст"},
		{"прикол", "зашёл", "л"},
	}
	for _, tc := range testcases {
		got, n := Suffix(tc.a, tc.b)
		rcWant := utf8.RuneCountInString(tc.want)
		rcGot := utf8.RuneCountInString(got)
		if n != rcGot || rcGot != rcWant || got != tc.want {
			t.Errorf("Suffix(%q,%q) = %d,%q, want %d,%q", tc.a, tc.b, n, got, rcWant, tc.want)
		}
	}
}

func TestSubstring(t *testing.T) {
	testcases := []struct {
		a, b string
		want string
	}{
		{"", "", ""},
		{"ф", "", ""},
		{"", "ф", ""},
		{"э", "ф", ""},
		{"эээ", "ф", ""},
		{"ф", "эээ", ""},
		{"эльф", "эй", "э"},
		{"ольга", "оля", "ол"},
		{"кадастры", "пиастр", "астр"},
		{"фантаст", "анталия", "анта"},
		{"прикол", "зашёл", "л"},
	}
	for _, tc := range testcases {
		got, n := Substring(tc.a, tc.b)
		rcWant := utf8.RuneCountInString(tc.want)
		rcGot := utf8.RuneCountInString(got)
		if n != rcGot || rcGot != rcWant || got != tc.want {
			t.Errorf("Substring(%q,%q) = %d,%q, want %d,%q", tc.a, tc.b, n, got, rcWant, tc.want)
		}
	}
}
